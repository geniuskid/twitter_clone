class User < ActiveRecord::Base
#   attr_accessible :avatar_url, :bio, :email, :name, :password_diggest, :username 
   
    has_secure_password
    
    Validates :email, presence: true, uniqueness: true, format: { with: /^[\w\.+-]+@([\w]+\.)+[a-zA-Z]}
    
    validates :username, presence: true, uniqueness: true
    
    validates :name, presence: true
#    Association
    
end
